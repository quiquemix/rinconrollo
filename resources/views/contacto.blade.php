@extends('layouts.app')

@section('content')

	<div class="card m-auto" style=" width: 80rem;">
	<div class="card-header">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2939.389611490678!2d-6.605353248795295!3d42.54701927907259!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd30baf277a52fbb%3A0x42fd242d78dfe00!2sAv.+Espa%C3%B1a%2C+37%2C+24400+Ponferrada%2C+Le%C3%B3n!5e0!3m2!1ses!2ses!4v1553040773388" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
  	</div>
  	<div class="card-body">
  	<ul class="list-group list-group-flush text-center">
    	<li class="list-group-item">Ponferrada, León</li>
    	<li class="list-group-item">Avenida de España 37, Bajo</li>
    	<li class="list-group-item">987678954</li>
    	<li class="list-group-item">elrincondelrollo@gmail.com</li>
    	<li class="list-group-item">Lunes a Viernes</li>
    	 <li class="list-group-item">Mañanas de 10:00 a 13:00</li>
    	<li class="list-group-item">Tardes de 16:30 a 21:00</li>
    	<li class="list-group-item">Sábados de 11:00 a 13:30</li>
  	</ul>
  </div>
</div>


@endsection