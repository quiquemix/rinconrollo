@extends('layouts.app')

@section('content')

<div class="card m-auto mb-3 text-center" style="width: 55rem;">
  <img src="/{{$zapa->rutaimg}}" class="card-img-top" alt="...">
  <div class="card-body">
    <h3 class="card-title">RESUMEN DE LA COMPRA</h3>
    <p class="card-text">Modelo: {{ App\Models\Modelo::where('id_zapatilla','=',$zapa->id_modelo )->value('description') }}</p>
    <p class="card-text">Talla: {{$zapa->talla}}</p>
    <p class="card-text">Precio: {{ App\Models\Modelo::where('id_zapatilla','=',$zapa->id_modelo )->value('precio') }} euros</p>

    <div id="paypal-button"></div>

  </div>
</div>



<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
  paypal.Button.render({
    // Configure environment
    env: 'sandbox',
    client: {
      sandbox: 'demo_sandbox_client_id',
      production: 'demo_production_client_id'
    },
    // Customize button (optional)
    locale: 'en_US',
    style: {
      size: 'responsive',
      color: 'blue',
      shape: 'rect',
    },

    // Enable Pay Now checkout flow (optional)
    commit: true,

    // Set up a payment
    payment: function(data, actions) {
      return actions.payment.create({
        transactions: [{
          amount: {
            total: '0.01',
            currency: 'USD'
          }
        }]
      });
    },
    // Execute the payment
    onAuthorize: function(data, actions) {
      return actions.payment.execute().then(function() {
        // Show a confirmation message to the buyer
        window.alert('Thank you for your purchase!');
      });
    }
  }, '#paypal-button');

</script>

@endsection