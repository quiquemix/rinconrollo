@extends('layouts.app')

@section('content')

<div class="card m-auto" style=" width: 80rem;">
	<div class="card-header text-center">
		<h2 class="titulohome">{{ Auth::user()->name }}</h2>
	</div>
  	<div class="card-body">
  	<ul class="list-group list-group-flush text-center">
  		<li class="list-group-item">{{ Auth::user()->email }}</li>
    	<li class="list-group-item">Fecha de registro: {{ Auth::user()->created_at }}</li>
    	<li class="list-group-item">Última actualización: {{ Auth::user()->updated_at }}</li>
  	</ul>
  	</div>
 </div>


@endsection