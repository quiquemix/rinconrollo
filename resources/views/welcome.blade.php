@extends('layouts.app')

@section('content')

<div class="card m-auto" style=" width: 80rem;">
  <div class="card-body">
    
    <div id="carouselExampleCaptions" class="carousel slide " data-ride="carousel">
	  <ol class="carousel-indicators">
	    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
	    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
	    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
	  </ol>
	  <div class="carousel-inner">
	    <div class="carousel-item active">
	      <img src="img/pumawelcome.jpg" class="d-block w-100" alt="...">
	      <div class="carousel-caption d-none d-md-block">
	        <h5><a class="nav-link" href="{{ route('zapatillas') }}">PUMA</a></h5>
	        <p>Nuestra mejor selección de Puma.</p>
	      </div>
	    </div>
	    <div class="carousel-item">
	      <img src="img/nikewelcome.png" class="d-block w-100" alt="...">
	      <div class="carousel-caption d-none d-md-block">
	      	 <h5><a class="nav-link" href="{{ route('zapatillas') }}">NIKE</a></h5>
	       
	        <p>LA CALIDAD NO SE PUEDE COMPRAR.</p>
	      </div>
	    </div>
	    <div class="carousel-item">
	      <img src="img/adidaswelcome.jpg" class="d-block w-100" alt="...">
	      <div class="carousel-caption d-none d-md-block">
	      	 <h5><a class="nav-link" href="{{ route('zapatillas') }}">ADIDAS</a></h5>
	        
	        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
	      </div>
	    </div>
	  </div>
	  <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
	    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
	    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
	</div>
  </div>
</div>

<p class="titulo_nav"></p>



@endsection