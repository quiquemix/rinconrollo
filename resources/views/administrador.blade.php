@extends('layouts.app')

@section('content')
   @if (\Session::has('successmsg'))
	    <div class="alert alert-success">
	        <ul>
	            <li>{{ Session::get('successmsg') }}</li>
	        </ul>
	    </div>
	@endif
<div class="card m-auto" style=" width: 80rem;">
	<div class="card-header text-center">
		<h2 class="titulohome">HERRAMIENTA DE ADMINISTRACIÓN</h2>
	</div>
  	<div class="card-body">
  	<ul class="list-group list-group-flush text-center">
  		@forelse($usuarios as $user)
  			@if($user->role === 'admin')
  				{{ $user->name }}
  				 --- ERES EL ADMINISTRADOR
  			@else
  			<li class="list-group-item">
  				{{ $user->name }}

			<a href="{{ route('borrarusuario', $user->id) }}"><button type="button" class="btn btn-danger">Eliminar</button></a>
  			</li>
  			@endif
    	@empty
		<p>No hay usuarios registrados</p>
		@endforelse	
  	</ul>
  	</div>
 </div>
@endsection