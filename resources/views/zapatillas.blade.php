@extends('layouts.app')

@section('content')
	
  <div class="d-flex align-items-center flex-wrap">
  @forelse($zapas as $zapa)
  <div class="mb-2 ml-2">
  <div class="card m-auto mb-3 text-center" style="width: 18rem;">
  <img src="{{ $zapa->rutaimg }}" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">{{ App\Models\Modelo::where('id_zapatilla','=',$zapa->id_modelo )->value('description') }}</h5>
    <p class="card-text">Talla: {{$zapa->talla}}</p>
    @if($zapa->stock === 0)
      <p class="text-danger">No hay stock</p>
      <p class="text-danger">Inténtalo más tarde</p>
    @else
      <a href="{{ route('pago', $zapa->id) }}">
      <button type="button" class="btn m-auto"><img width="68px" height="68px" src="/img/buy.png" alt="..."></button>
      </a>
    @endif  
  </div>
  </div>
</div>
  @empty
  		<p>No hay productos para mostrar</p>
  @endforelse
 </div> 


@endsection