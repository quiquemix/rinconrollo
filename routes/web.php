<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//RUTAS PRINCIPALES
Route::get('/', 'App\Http\Controllers\MainController@welcome')->name('welcome');
Route::get('/home', 'App\Http\Controllers\MainController@home')->name('home')->middleware('App\Http\Middleware\Authenticate');;
Route::get('/modelo', 'App\Http\Controllers\MainController@modelo')->name('modelo');
Route::get('/contacto', 'App\Http\Controllers\MainController@contacto')->name('contacto');

//RUTAS DE REDIRECCIÓN
Route::get('/zapatillas', 'App\Http\Controllers\MainController@zapatillas')->name('zapatillas');
Route::get('/pago/{id}', 'App\Http\Controllers\MainController@pago')->name('pago');

//RUTA DEL ADMINISTRADOR DEL SITIO WEB
Route::get('/administrador', 'App\Http\Controllers\MainController@administrador')->name('administrador')->middleware('App\Http\Middleware\IsAdmin');
Route::get('/borrarusuario/{id}', 'App\Http\Controllers\MainController@borrarusuario')->name('borrarusuario')->middleware('App\Http\Middleware\IsAdmin');
//RUTAS DEL LOGIN
Route::get('login', 'App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'App\Http\Controllers\Auth\LoginController@login');
Route::post('logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout');

// RUTAS DEL REGISTRO
Route::get('register', 'App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'App\Http\Controllers\Auth\RegisterController@register');