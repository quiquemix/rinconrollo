<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Zapatilla;
use App\Models\User;
use App\Models\Modelo;

class MainController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }

    public function modelo()
    {
        return view('modelo');
    }

    public function contacto()
    {
        return view('contacto');
    }

    public function home()
    {
        return view('home');
    }

    public function zapatillas()
    {
        return view('zapatillas', [
            'zapas' => Zapatilla::all(),
        ]);
    }

    public function pago($id)
    {
        $zapa = Zapatilla::where('id','=',$id)->firstOrFail();
        return view('pago', [
            'zapa' => $zapa,
        ]);
    }

    public function administrador()
    {
        return view('administrador', [
            'usuarios' => User::all(),
        ]);
    }

    public function borrarusuario($id)
    {

        $user = User::where('id','=',$id)->firstOrFail();
        $user->delete();

        return redirect()->back()->with('successmsg', 'El usuario se ha borrado correctamente');

    }
}
