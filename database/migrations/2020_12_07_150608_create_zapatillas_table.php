<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZapatillasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zapatillas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('id_modelo');
            $table->tinyInteger('talla');
            $table->tinyInteger('stock');
            $table->mediumText('rutaimg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zapatillas');
    }
}
