<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ str_replace('_', ' ', config('app.name')) }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>


    <!-- Estilos -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <!-- Barra de navegación -->
        <nav class="navbar navbar-expand-lg navbar-light titulo_nav" style="background-color: #e3f2fd;">
          <a class="navbar-brand" href="{{ route('welcome') }}">RINCÓN DEL ROLLO</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse flex-grow-1" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
              <!-- Condición de registro en sesión de laravel -->
              @guest
              <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('register') }}">Registro</a>
              </li>

              @else

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{ Auth::user()->name }}
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="{{ route('home') }}">Perfil</a>

                  @if (auth()->user()->role == 'admin')
                    <a class="dropdown-item" href="{{ route('administrador') }}">Gestionar web</a>
                  @endif
                  
                  <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    Cerrar sesión
                  </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
              </li>

              @endguest

              <li class="nav-item">
                <a class="nav-link" href="{{ route('zapatillas') }}">Zapatillas</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('contacto') }}">Contacto</a>
              </li>
            </ul>
          </div>
        </nav>

    <!-- Contenido de cada vista -->
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <!-- Footer -->
    <footer class="site-footer bg-info text-dark">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>SOBRE NOSOTROS</h6>
            <p class="text-justify"><i>El Rincón del rollo</i> es una página web de réplicas de primera calidad. Contamos con los mejores proveedores del territorio nacional e internacional. Envíos a toda España inicialmente, con proyección a ventas internacionales.</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>MÉTODOS DE PAGO</h6>
            <ul class="footer-links text-dark">
              <li>Paypal</li>
              <li>Contrareembolso</a></li>
              <li>Tarjeta de Crédito</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Mapa Web</h6>
            <ul class="footer-links">
              <li><a href="{{ route('welcome') }}">Inicio</a></li>
              <li><a href="{{ route('zapatillas') }}">Zapatillas</a></li>
              <li><a href="{{ route('contacto') }}">Contacto</a></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2020 All Rights Reserved by Enrique Manuel Gómez Murciego.
            </p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="https://twitter.com/?lang=es"><i class="fa fa-twitter"></i></a></li>
              <li><a class="bg-warning" href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a></li>   
            </ul>
          </div>
        </div>
      </div>
    </footer>

</body>
</html>
